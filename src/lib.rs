use std::str::FromStr;
use std::{env, process};

pub mod cd;
pub mod hint;
pub mod matcher;
pub mod shell;

use matcher::Matcher;

/// Number or String input
#[derive(Clone, Debug)]
pub enum NumberOrString {
    Number(u8),
    Str(String),
}

/// Create instance from string argument
impl FromStr for NumberOrString {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(s.parse::<u8>()
            .map(NumberOrString::Number)
            .unwrap_or_else(|_| NumberOrString::Str(s.to_string())))
    }
}

/// Run cd command
pub fn run_cd(target: &NumberOrString) {
    let matcher = Matcher::default();
    let cwd = env::current_dir().expect("Could not get current working directory");
    let new_path = match target {
        NumberOrString::Number(level) => cd::up_by_count(&cwd, *level as usize),
        NumberOrString::Str(component) => cd::up_to_target(&matcher, &cwd, component),
    };

    match new_path {
        Ok(new_path) => {
            println!("{}", new_path.display());
        }
        Err(e) => {
            eprintln!("{}", e);
            println!("{}", cwd.display());
            process::exit(1);
        }
    }
}

/// Run hint command
pub fn run_hint(term: Option<&str>) {
    let matcher = Matcher::default();
    let cwd = env::current_dir().expect("Could not get current working directory");
    let hint = hint::completion_for(&matcher, &cwd, term);

    println!("{}", hint);
}

/// Run init command
pub fn run_init(alias: &String, hint: &Option<String>) {
    match shell::initialize(alias.to_string(), hint) {
        Ok(shell) => println!("{}", shell.compile_script()),
        Err(e) => {
            println!("{e}");
            process::exit(1);
        }
    }
}
