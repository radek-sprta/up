use std::path::Path;

use crate::matcher::Matcher;

/// Create completion hint for given directory and term. If term is None, return
/// all components of the directory. If term is Some, return the directory
/// component that is the closest fuzzy match to the term.
///
/// # Examples
///
/// ## For Some term
///
/// ```
/// let matcher = cdup::matcher::Matcher::default();
/// let cwd = std::path::Path::new("/home/rsprta/games/angband");
/// let term = Some("ang");
/// let expected = String::from("angband");
///
/// assert_eq!(expected, cdup::hint::completion_for(&matcher, &cwd, term));
/// ```
///
/// ## For None term
/// ```
/// let matcher = cdup::matcher::Matcher::default();
/// let cwd = std::path::Path::new("/home/rsprta/games/angband");
/// let term = None;
/// let expected = String::from("/ home rsprta games angband");
///
/// assert_eq!(expected, cdup::hint::completion_for(&matcher, &cwd, term));
/// ```
pub fn completion_for(matcher: &Matcher, directory: &Path, term: Option<&str>) -> String {
    let components: Vec<&str> = directory
        .components()
        .map(|c| c.as_os_str().to_str().unwrap())
        .collect();

    match term {
        Some(term) => {
            let best_match = matcher.best_match(&components, term);
            match best_match {
                Some(index) => components[index].to_string(),
                None => String::new(),
            }
        }
        None => components.join(" "),
    }
}
