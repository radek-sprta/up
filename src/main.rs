use clap::{Parser, Subcommand};

#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,

    /// Turn debugging information on
    #[arg(short, long, action = clap::ArgAction::Count)]
    debug: u8,
}

#[derive(Subcommand)]
enum Commands {
    /// Ascend directory by count or path component
    Cd {
        /// Number of levels to ascend or directory to ascend to
        target: cdup::NumberOrString,
    },
    /// Give completion hint for given term
    Hint {
        /// Term to print hint for
        term: Option<String>,
    },
    /// Initialize function for shell
    Init {
        /// Function name to use
        #[arg(short, long, default_value = "up")]
        alias: String,
        /// Used shell
        hint: Option<String>,
    },
}

fn main() {
    let cli = Cli::parse();

    match &cli.command {
        Some(Commands::Cd { target }) => cdup::run_cd(target),
        Some(Commands::Hint { term }) => cdup::run_hint(term.as_deref()),
        Some(Commands::Init { alias, hint }) => cdup::run_init(alias, hint),
        None => {}
    }
}
