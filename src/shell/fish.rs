/// Integration script template for fish
pub const SCRIPT: &str = "\
# Put the line below in ~/.config/fish/config.fish:
#
#   cdup init fish | source

function {alias}
  test -z \"$argv\"; and set argv 1
  set -l dir (command cdup cd \"$argv\")
  test -d \"$dir\"; and cd \"$dir\"
end

function __cdup_hint
    set -l term (string replace -r '^{alias} ' '' -- (commandline -cp))
    if test -z \"$term\";
        command cdup hint
    else
        command cdup hint \"$term\"
    end
end

complete --command {alias} --exclusive --arguments (__cdup_hint)
";
