/// Integration script template for Powershell
pub const SCRIPT: &str = "\
# Put the line below in your PowerShell profile (Microsoft.PowerShell_profile.ps1):
#
#   Invoke-Expression (&cdup init pwsh | Out-String)
#
# You can locate your profile by typing $PROFILE in PowerShell.
function {alias}
{
    $Input = $Args[0] ?? '1';
    $Path = $(cdup cd $Input);
    if (Test-Path $Path)
    {
        Set-Location $Path;
    }
}

if (Get-Command -Name Register-ArgumentCompleter -ErrorAction Ignore) {
    Register-ArgumentCompleter -CommandName '{alias}' -ScriptBlock {
        param ($commandName, $wordToComplete, $commandAst, $fakeBoundParameter)

        $wordToComplete = \"$wordToComplete\"
        if ($wordToComplete.StartsWith('{alias} ')) {
            $wordToComplete = \"$wordToComplete\".Substring('{alias} '.Length)
        }

        cdup hint $wordToComplete | ForEach-Object {
            if ($_.Contains(' ')) { \"'\" + $_ + \"'\" } else { $_ }
        }
    }
}
";
