/// Integration script template for zsh
pub const SCRIPT: &str = "\
# Put the line below in ~/.zshrc:
#
#   eval \"$(cdup init zsh)\"

{alias}() {
  local dir=$(command cdup cd \"${*:-1}\")
  test -d \"$dir\" && cd \"$dir\"
}

{alias}_completion() {
  local term=\"${1/#{alias} /}\"
  if [[ -z \"${term}\" ]]; then
    reply=($(command cdup hint))
  else
    reply=($(command cdup hint \"${term}\"))
  fi
}

compctl -U -K {alias}_completion {alias}
";
