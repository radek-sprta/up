/// Integration script template for bash
pub const SCRIPT: &str = "\
# Put the line below in ~/.bashrc or ~/bash_profile:
#
#   eval \"$(cdup init bash)\"

{alias}() {
  local dir=$(command cdup cd \"${*:-1}\")
  test -d \"$dir\" && cd \"$dir\"
}

__cdup_hint() {
  local term=\"${COMP_LINE/#{alias} /}\"
  if [[ -z ${term} ]]; then
      COMPREPLY=($(command cdup hint))
  else
      COMPREPLY=($(command cdup hint \"$term\"))
  fi
}

complete -F __cdup_hint {alias}
";
