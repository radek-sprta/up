# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0](https://gitlab.com/radek-sprta/cdup/compare/v0.4.0...v0.5.0) - 2023-09-24

### Added

- return all segments when hint is empty

### Removed

- \[**breaking**\] remove shell subcommand

### Other

- Fix lcov_cobertura install

## [0.4.0](https://gitlab.com/radek-sprta/cdup/compare/v0.3.0...v0.4.0) - 2023-08-19

### Added

- support `powershell` as hint
- \[**breaking**\] change `shell` command to `init`
- show error message when no match found
- support powershell

### Fixed

- *(shell)* accept multiple arguments
- upload coverage only in main

### Other

- add doc comments for all public code
- upload coverage reports to Gitlab pages
- add code coverage

## [0.3.0](https://gitlab.com/radek-sprta/cdup/compare/v0.2.0...v0.3.0) - 2023-08-01

### Added

- support calling alias without argument
- Add completion hints for shell
- add fuzzy match support for target
- support string input for cd

### Other

- add animation showing usage
- move cd module

## [0.2.0](https://gitlab.com/radek-sprta/cdup/compare/v0.1.0...v0.2.0) - 2023-07-03

### Added

- support custom alias for ascend function
- support zsh

### Fixed

- fix binary name in shell script

### Other

- fix badge URLs
- add changelog
- add badges to readme
- pre-commit config for markdown
- add justfile

## 0.1.0 - 2023-07-02

### Added

- support ascending directory in fish & bash
