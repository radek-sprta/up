# cdup

[![Version](https://img.shields.io/crates/v/cdup.svg)](https://crates.io/crates/cdup)
[![Downloads](https://img.shields.io/crates/d/cdup.svg)](https://crates.io/crates/cdup)
[![License](https://img.shields.io/crates/l/cdup.svg)](https://crates.io/crates/cdup)
[![Coverage report](https://gitlab.com/radek-sprta/cdup/badges/main/coverage.svg)](https://gitlab.com/radek-sprta/cdup/-/commits/main)

cdup is a CLI utility to ascend directories by count.
It currently ships with integrations for the following shells:

- bash
- fish
- powershell
- zsh

![](static/animation.svg)

## Installation

You can install cdup by building it with cargo. Run the following command:

`cargo install --target-dir ~/.local cdup`

This will create a binary in `~/.local/bin/cdup`.

## Usage

To get the run shell-specific instructions, use the following command:

`cdup init`

That will print instructions on how to add the `up` integration function to
your shell.

Afterward, you can call `up` with a number to ascend the desired number of directories.
Here's an example:

```sh
$ pwd
/home/rsprta/git/cdup
$ up
$ pwd
/home/rsprta/git
$ up rs
$ pwd
/home/rsprta
$ up 2
$ pwd
/
```

## Acknowledgements

- cdup uses the excellent [clap](https://crates.io/crates/clap) library to
  create the command-line interface.
- The terminal animation was created with [Asciinema](https://asciinema.org/) and
  [svg-term-cli](https://github.com/marionebl/svg-term-cli).

## License

GNU General Public License v3.0
