set positional-arguments

default:
    @just --list

build:
    cargo build

changelog:
    release-plz update

format:
    cargo fmt

lint:
    pre-commit run --all

release:
    cargo publish

run *args:
    cargo run -- {{args}}

test:
    cargo test

update:
    cargo update
